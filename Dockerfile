# syntax=docker/dockerfile:1
FROM rustlang/rust:nightly-bullseye as build

RUN mkdir /gnarlo-rust
WORKDIR /gnarlo-rust

COPY Cargo.lock .
COPY Cargo.toml .

ENV CARGO_HOME=/root/.cargo

RUN --mount=type=cache,target=/root/.cargo \
      mkdir src/ && touch src/lib.rs && cargo fetch  && rm -rf src/

COPY . .

RUN --mount=type=cache,target=/root/.cargo \
    cargo build --release

FROM debian:bullseye

RUN mkdir /app

WORKDIR /app

COPY --from=build /gnarlo-rust/target/release/gnarlo /app/

ENTRYPOINT ["/app/gnarlo"]
