# Gnarlo - An Arlo RPC daemon in rust

## Why

I don't like being forced to use Arlo's cloud to watch cameras 10 feet from my computer.  Gnarlo mimics the
bare minimum functionality to cause the cameras to believe they are operating with an official base station.

I couldn't think of a more complex way to do this.  I needed to understand rust, so what a better way to do that, than
to write some really cruddy rust code to complicate matters.  

Next will be an RTSP proxy since the cameras only support a single RTSP client - and, surprise, are highly picky
with the clients.

## Docker builds

To build:
```
docker buildx build --progress=plain  -t gnarlo-rust ./
```

To run:
````
 docker run --rm -it --name=gnarlo-rust -p 4000:4000 -p 8000:8000 gnarlo-rust -r 0.0.0.0:4000 -h 0.0.0.0:8000 -v
````


## Protocol

Some custom TCP protocol I have never seen before.  A breakdown of the messaging protocol is in PROTOCOL.md
