# Arlo "Authentication"

The cameras perform a short sequence of connections and send some json data to the base station or they will de-associate from the AP.

Requester opens tcp:4000 to destination and pushes json data, the destination replies back with Ack, connection closed by requester.

## Message Format

Request: `L:<length> {"Type":"$Type","ID":$ID, $JSON}` : $Type is the procedure and $ID seems to be anumeric identifer of $Type
Response: `L:<length>  {"Type":"response","Response":"Ack","ID":$ID}` $ID from the request.

## Message Types

### registration

**Camera** -> Base station

```
L:773 {"Type":"registration","ID":46,"SystemSerialNumber":"XXXXXXXXXXXX","SystemModelNumber":"VMC4030","SystemFirmwareVersion":"1.092.0.28_56_991","UpdateSystemModelNumber":"VMC4030","CommProtocolVersion":1,"BatPercent":0,"SignalStrengthIndicator":5,"LogFrequency":2,"BatTech":"None","ChargerTech":"QuickCharger","ChargingState":"Off","ThermalShutdownRechargeMaxTemp":60,"Temperature":40,"InterfaceVersion":2,"Capabilities":["IRLED","PirMotion","NightVision","Temperature","BatteryLevel","Microphone","Speaker","SignalStrength","Solar","BatteryCharging","H.264Streaming","JPEGSnapshot","AutomatedStop","BEC","RaParams"],"HardwareRevision":"H8","Sync":true,"BattChargeMinTemp":0,"BattChargeMaxTemp":60,"ThermalShutdownMinTemp":-20,"ThermalShutdownMaxTemp":74,"BootSeconds":26845}
```

Base station response:

```
L:44 {"Type":"response","ID":46,"Response":"Ack"}
```

### registerSet

**Base station** -> Camera

```
L:766 {"Type":"registerSet","ID":26,"SetValues":{"VideoExposureCompensation":0,"VideoMirror":false,"VideoFlip":false,"VideoWindowStartX":0,"VideoWindowStartY":0,"VideoWindowEndX":1280,"VideoWindowEndY":720,"MaxMissedBeaconTime":10,"MaxStreamTimeLimit":1800,"VideoAntiFlickerRate":60,"WifiCountryCode":"US","NightVisionMode":true,"HdrControl":"auto","MaxUserStreamTimeLimit":1800,"MaxMotionStreamTimeLimit":120,"VideoMode":"superWide","JPEGOutputResolution":"","ChargeNotificationLed":1,"AudioMicAGC":0,"VideoOutputResolution":"720p","VideoTargetBitrate":450,"Audio0EncodeFormat":0,"Audio1EncodeFormat":1,"ArloSmart":false,"AlertBackoffTime":0,"PIRTargetState":"Disarmed","AudioTargetState":"Disarmed","VideoMotionEstimationEnable":false,"DefaultMotionStreamTimeLimit":10}}
```

Camera response:

```
L:44 {"Type":"response","Response":"Ack","ID":26}
```

### status

Camera -> Base station

```
L:956 {"Type":"status","ID":47,"SystemFirmwareVersion":"1.092.0.28_56_991","HardwareRevision":"H8","SystemSerialNumber":"XXXXXXXXXXXX","UpdateSystemModelNumber":"VMC4030","WifiCountryDetails":"US/912","BatPercent":0,"BatTech":"None","ChargerTech":"QuickCharger","ChargingState":"Off","Bat1Volt":-1.000000000,"Temperature":40,"Battery1CaliVoltage":-1.000000000,"SignalStrengthIndicator":5,"Streamed":2006,"UserStreamed":2006,"MotionStreamed":0,"IRLEDsOn":2006,"PoweredOn":26849,"CameraOnline":4991,"CameraOffline":21858,"WifiConnectionCount":15,"WifiConnectionAttempts":593,"PIREvents":0,"FailedStreams":0,"FailedUpgrades":0,"SnapshotCount":0,"LogFrequency":2,"CriticalBatStatus":0,"ISPOn":23645,"TimeAtPlug":2150,"TimeAtUnPlug":0,"PercentAtPlug":0,"PercentAtUnPlug":0,"ISPWatchdogCount":0,"ISPWatchdogCount2":4,"SecsPerPercentCurr":0,"SecsPerPercentAvg":0,"PirOorEvents":0,"DdrFailCnt":0,"DhcpFCnt":36,"RegFCnt":157,"TxErr":0,"TxFail":0,"TxPhyE1":6,"TxPhyE2":0}
```

Base station response:

```
L:44 {"Type":"response","ID":47,"Response":"Ack"}
```

### raParams

**Base station** -> Camera:

```
L:254 {"Type":"raParams","ID":27,"Params":{"360p":{"minbps":51200,"maxbps":409600,"minQP":24,"maxQP":38,"vbr":true,"targetbps":204800,"cbrbps":204800},"720p":{"minbps":51200,"maxbps":599040,"minQP":24,"maxQP":38,"vbr":true,"targetbps":460800,"cbrbps":460800}}}
```

Camera response:

```
L:44 {"Type":"response","Response":"Ack","ID":27}
```

### statusRequest

**Base station** -> Camera:

```
L:32 {"Type":"statusRequest","ID":28}
```

Camera response:

```
L:44 {"Type":"response","Response":"Ack","ID":28}
```

The camera then responds back with an additional status message.

### registerSet (UserStreamActive)


**Base station** -> Camera:

```
L:65 {"Type":"registerSet","ID":29,"SetValues":{"UserStreamActive":1}}
```

Camera response:

```
L:44 {"Type":"response","Response":"Ack","ID":29}
```

This is not even required - you can just connect straight to the IP normally with rtsp.  Nothing is needed to activate it.

`rtsp://192.168.10.174`

The RTSP/RTP is below, but seems "standard" enough for VLC to just work out of the box.

### registerSet (PIRTargetState)

```
L:102 {"Type":"registerSet","ID":66,"SetValues":{"PIRTargetState":"Disarmed","AudioTargetState":"Disarmed"}}
```

## TBD

### upgrade

BaseStation -> Camera

```
{"Type":"upgrade","ID":44,"SystemFirmwareVersion":"1.092.0.28_56_991","FileSize":3171808,"FileURL":"http://172.14.1.1/upgrade/2W5uGnqvOqMVa1.bin"}
```

### fullSnapshot

BaseStation -> Camera

```
{"Type":"fullSnapshot","DestinationURL":"http://urlToUpload"}
```
From https://github.com/KhaosT/Arlo-base/blob/master/arlo.js

The image is posted to the specified URL.

The image data is between the first `\r\n\r\n` and a sequence of dashes `--------------------`

It appears that the response needs to contain some markup

```
<html><body>The upload has been completed.</body></html>
```

# RTSP/RTP

After the base station notifies the camera via the registerSet that it will be activating a stream, the base station then initiates an RTSP session.  This is just an observation of the behavior of the two - the camera can be streamed directly without any interaction.

## OPTIONS
```
OPTIONS rtsp://172.14.1.127/live RTSP/1.0
```

## DESCRIBE
```
DESCRIBE rtsp://172.14.1.127/live RTSP/1.0
```

Some data probably used in following setup.  Clearly not the session id...

## SETUP

```
SETUP rtsp://172.14.1.127/live/track2 RTSP/1.0
CSeq: 3
Transport: RTP/AVP;unicast;client_port=1150-1151

RTSP/1.0 200 OK
CSeq: 3
Transport: RTP/AVP/UDP;unicast;client_port=1150-1151;server_port=45610-45611
Session: 1234abcd
```

I saw track2,1,3, in that order, where the strong crypto session ID was returned on the first call to track2.

## PLAY

```
PLAY rtsp://172.14.1.127/live RTSP/1.0
CSeq: 6
Session: 1234abcd
Range: npt=0.000-

RTSP/1.0 200 OK
CSeq: 6
Rangle:npt=0.000-
Session: 1234abcd
RTP-INFO:url=rtsp://172.14.1.127/live/track1;seq=0;rtptime=0;url=rtsp://172.14.1.127/live/track2;seq=0;rtptime=0
;url=rtsp://172.14.1.127/live/track3;seq=0;rtptime=0
```

# Finale

I see nothing preventing a simple TCP:4000 service to receive and return the json to keep the cameras happy and online.

I see nothing preventing a simple service to activate the stream over port 4000, and then connecting via any standard RTSP player like VLC.
