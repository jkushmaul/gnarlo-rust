#![feature(io_error_more)]

extern crate core;

use env_logger::Env;
use log::{debug, info};

use crate::api::GnarloApi;
use crate::gnarlo::GnarloRpc;
use crate::gnarlo::{GnarloHttpService, GnarloMsg};
use crate::repository::GnarloRepository;
use crate::rpc::{FrameMessage, Server, TcpServer};
use std::net::SocketAddr;

mod api;
mod gnarlo;
mod repository;
mod rpc;

use crate::gnarlo::GnarloCodecFactory;
use clap::Parser;
use std::process::exit;
use tokio_cron_scheduler::{Job, JobScheduler};

/// An RPC daemon which can replace the Arlo base station for Arlo Pro cameras
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Opts {
    /// The Arlo TCP IP:PORT to listen on
    #[clap(short = 'r', long, default_value = "127.0.0.1:4000")]
    rpc_bind: String,
    /// The HTTP IP:PORT to listen on
    #[clap(short = 'l', long, default_value = "127.0.0.1:8000")]
    http_bind: String,
    /// The cron for heartbeats
    #[clap(short = 'c', long, default_value = "1 */5 * * * *")]
    heartbeat_cron: String,
    /// Turn on debug logging
    #[clap(short = 'v', long)]
    verbose: bool,
}

#[tokio::main]
async fn main() {
    ctrlc::set_handler(move || exit(1)).expect("Error setting Ctrl-C handler");

    let opts: Opts = Opts::parse();
    let log_level = match opts.verbose {
        true => "debug",
        false => "info",
    };

    env_logger::Builder::from_env(Env::default().default_filter_or(log_level)).init();
    debug!("parsing addresses");
    let http_bind_addr: SocketAddr = opts.http_bind.parse().expect("Invalid http bind addr");
    let rpc_bind_addr: SocketAddr = opts.rpc_bind.parse().expect("Invalid rpc bind addr");

    debug!("Initializing");
    let repo: GnarloRepository = repository::GnarloRepository::new();
    let api = GnarloApi::new(repo);
    let http_service = GnarloHttpService::new(api.clone());

    log::debug!("Launching services");
    let codec = GnarloCodecFactory {};
    const QUEUE_SIZE: usize = 1024;
    let (egress_tx, egress_rx) = tokio::sync::mpsc::channel::<FrameMessage<GnarloMsg>>(QUEUE_SIZE);
    let rpc_service = GnarloRpc::new(egress_tx, api);

    let (cron_tx, cron_rx) = tokio::sync::mpsc::channel::<String>(QUEUE_SIZE);
    let rpc_server = TcpServer::new(codec, rpc_service, egress_rx, cron_rx);
    log::debug!("Creating heartbeat schedule");
    let sched = JobScheduler::new().expect("Could not create scheduler");

    sched
        .add(
            Job::new(opts.heartbeat_cron.as_str(), move |_, _| {
                match cron_tx.try_send("refresh_status".to_string()) {
                    Ok(_) => {
                        log::debug!("Heartbeat signal sent");
                    }
                    Err(e) => {
                        log::error!("Error sending heatbeat signal {}", e);
                    }
                };
            })
            .expect("Could not create job"),
        )
        .expect("Could not add job");
    sched.start().expect("Could not start scheduler");

    tokio::select! {
        _ = http_service.run(http_bind_addr) => {
            info!("Http service exited");
        }
        _ = rpc_server.run(rpc_bind_addr) => {
            info!("Rpc service exited");
        }
    };
    info!("Terminating");
}
