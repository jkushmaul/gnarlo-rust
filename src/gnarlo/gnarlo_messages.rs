use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use std::io::{Error, ErrorKind, Result};

pub const SERIAL_KEY: &str = "SystemSerialNumber";

#[derive(Serialize, Deserialize, Debug)]
pub struct ResponseType {
    #[serde(rename = "Type")]
    pub req_type: String,
    #[serde(rename = "ID")]
    pub id: u32,
    #[serde(rename = "Response")]
    pub response: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RequestType {
    #[serde(rename = "Type")]
    pub req_type: String,
    #[serde(rename = "ID")]
    pub id: u32,
    pub metadata: Option<Map<String, Value>>,
}

impl ResponseType {
    pub fn new(id: u32) -> Self {
        let response = "Ack".to_string();
        return ResponseType {
            req_type: "response".to_string(),
            id,
            response,
        };
    }
    pub fn to_json(&self) -> serde_json::Result<String> {
        return serde_json::to_string(self);
    }
}

impl RequestType {
    pub fn new(req_type: String, id: u32, metadata: Option<Map<String, Value>>) -> Self {
        return RequestType {
            req_type,
            id,
            metadata,
        };
    }

    pub fn get_serial(&self) -> Option<&str> {
        if let Some(metadata) = self.metadata.as_ref() {
            if let Some(value) = metadata.get(SERIAL_KEY) {
                if let Some(vstr) = value.as_str() {
                    return Some(vstr);
                }
            }
        }
        return None;
    }

    pub fn parse_json(json: &str) -> Result<RequestType> {
        let data: Map<String, Value> = match serde_json::from_str::<Map<String, Value>>(json) {
            Ok(d) => d,
            Err(e) => {
                return Err(Error::new(ErrorKind::InvalidData, e));
            }
        };

        let req_type: String = data["Type"].as_str().unwrap_or("").to_string();
        let id: u32 = data["ID"].as_u64().unwrap_or(0) as u32;

        return Ok(RequestType::new(req_type, id, Some(data)));
    }

    pub fn to_json(&self) -> serde_json::Result<String> {
        return serde_json::to_string(self);
    }
}
