use async_trait::async_trait;
use std::io::{Error, ErrorKind, Result};
use std::net::SocketAddr;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

use crate::rpc::{FrameCodec, FrameCodecFactory, FrameMessage, FrameStream};
use bytes::BufMut;

#[derive(Clone, Debug)]
pub struct GnarloMsg {
    data: String,
}

impl GnarloMsg {
    pub(crate) fn to_str(&self) -> &str {
        return self.data.as_str();
    }
}

impl GnarloMsg {
    pub fn new(data: String) -> Self {
        Self { data }
    }
}

// The max premable would be: 'L:65535 {}'
const PREAMBLE_LEN: usize = 2 + 5 + 3;

pub struct GnarloMsgCodec<S>(S)
where
    S: AsyncRead + AsyncWrite + Unpin + Send;

impl<S> GnarloMsgCodec<S>
where
    S: AsyncRead + AsyncWrite + Unpin + Send,
{
    async fn begin_read_preamble(&mut self) -> Result<Vec<u8>> {
        // A (valid) message will always contain at least PREAMBLE_LEN bytes.
        // possible that it could be 'L:2 {}' but a valid message will never be that short.
        // read the max preamble length or fail
        let mut msg = vec![0u8; PREAMBLE_LEN];
        log::debug!("Reading preamble");
        let len_read = self.0.read_exact(msg.as_mut_slice()).await?;
        log::debug!("len_read: {}, msg_len: {}", len_read, msg.len());

        if len_read != msg.capacity() {
            return Err(Error::new(
                ErrorKind::InvalidData,
                "Your packet so short, it can't even contain a preamble",
            ));
        }
        // A valid message will always begin with L:
        if !msg.starts_with(b"L:") {
            return Err(Error::new(
                ErrorKind::InvalidData,
                "Message preamble was not found",
            ));
        }
        Ok(msg)
    }

    async fn finish_read_message(&mut self, mut full_msg: Vec<u8>) -> Result<Vec<u8>> {
        let remaining_len = full_msg.capacity() - full_msg.len();
        log::debug!("remaining_len: {}", remaining_len);

        if remaining_len > 1 {
            let mut buf = vec![0u8; remaining_len];
            let buf = buf.as_mut_slice();
            let n = self.0.read_exact(buf).await?;
            full_msg.put(&buf[..n]);
        }

        Ok(full_msg)
    }
}

pub struct GnarloCodecFactory;

impl<S> FrameCodecFactory<S, GnarloMsgCodec<S>, GnarloMsg> for GnarloCodecFactory
where
    S: FrameStream,
{
    fn create_codec(&self, stream: S) -> Result<GnarloMsgCodec<S>> {
        Ok(GnarloMsgCodec(stream))
    }
}

#[async_trait]
impl<S> FrameCodec<GnarloMsg> for GnarloMsgCodec<S>
where
    S: AsyncRead + AsyncWrite + Unpin + Send,
{
    async fn shutdown(mut self) -> Result<()> {
        self.0.shutdown().await
    }

    async fn read_frame(&mut self, peer: SocketAddr) -> Result<FrameMessage<GnarloMsg>> {
        let preamble = self.begin_read_preamble().await?;
        let full_msg = create_message_buffer(preamble)?;
        let full_msg = self.finish_read_message(full_msg).await?;

        let json = match String::from_utf8(full_msg.to_vec()) {
            Ok(j) => j,
            Err(e) => {
                return Err(Error::new(ErrorKind::InvalidData, e));
            }
        };
        log::debug!("read packet with len {}: {}", full_msg.len(), json);

        let msg = GnarloMsg::new(json);
        let frame = FrameMessage::new(msg, peer, true);
        return Ok(frame);
    }

    async fn write_frame(&mut self, frame: FrameMessage<GnarloMsg>) -> std::io::Result<()> {
        let msg = frame.get_msg();
        let peer = frame.get_peer();

        let json = msg.data.as_str();

        let full_msg = format!("L:{} {}", json.len(), json);
        log::debug!("writing frame to {}: {}", peer, full_msg);
        let full_msg = full_msg.as_bytes();
        self.0.write_all(full_msg).await?;
        return Ok(());
    }
}

fn create_message_buffer(preamble: Vec<u8>) -> Result<Vec<u8>> {
    let mut expected_len: usize = 0;
    let mut byte_index: usize = 0;
    let msg_str = match String::from_utf8(preamble.clone()) {
        Ok(m) => m,
        Err(e) => {
            return Err(Error::new(ErrorKind::InvalidData, e));
        }
    };

    log::debug!("Packet marker 'L:' present for preamble: {}", msg_str);

    for i in 2..preamble.len() {
        byte_index = i;
        let byte = preamble[byte_index];
        if byte == b' ' {
            log::debug!("Have space, breaking");
            break;
        } else if byte >= b'0' && byte <= b'9' {
            expected_len = expected_len * 10 + ((byte - b'0') as usize);
            log::debug!(
                "Have digit: {}, incrementing expected_len: {}",
                byte,
                expected_len
            );
            continue;
        } else {
            return Err(Error::new(
                ErrorKind::InvalidData,
                "Message preamble length was invalid",
            ));
        }
    }
    byte_index = byte_index + 1;

    let mut full_msg: Vec<u8> = Vec::with_capacity(expected_len);
    if byte_index < preamble.len() {
        let mut remaining = Vec::from(&preamble[byte_index..]);
        full_msg.append(&mut remaining);
    }
    Ok(full_msg)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_begin_read_preamble_will_fail_with_short_preamble() {
        let preamble = "L:1 ";
        let reader = tokio_test::io::Builder::default()
            .read(preamble.as_bytes())
            .build();
        let mut codec = GnarloMsgCodec(reader);
        let result = codec.begin_read_preamble().await;
        assert!(matches!(result, Err(_)));
    }

    #[tokio::test]
    async fn test_begin_read_preamble_will_fail_with_long_preamble_invalid_prefix() {
        let preamble = "X:65535 {}";
        let reader = tokio_test::io::Builder::default()
            .read(preamble.as_bytes())
            .build();
        let mut codec = GnarloMsgCodec(reader);
        let result = codec.begin_read_preamble().await;
        assert!(matches!(result, Err(_)));
    }

    #[tokio::test]
    async fn test_begin_read_preamble_will_pass() {
        let msg = "L:65535 {}";
        let reader = tokio_test::io::Builder::default()
            .read(msg.as_bytes())
            .build();

        let mut codec = GnarloMsgCodec(reader);
        let result = codec.begin_read_preamble().await.unwrap();
        assert_eq!(result.len(), PREAMBLE_LEN);
    }

    #[tokio::test]
    async fn test_create_message_buffer_pass_with_valid_length() {
        let orig = vec![b'X'; 32];
        let msg = String::from_utf8(orig.clone()).unwrap();
        let msg = format!("L:{} {}", msg.len(), msg);
        let buf = Vec::from(msg.as_bytes());
        let result = create_message_buffer(buf).unwrap();
        assert_eq!(result.len(), orig.len());
        assert_eq!(result, orig);
    }

    #[tokio::test]
    async fn test_create_message_buffer_fails_with_invalid_length() {
        let msg = "X:6553a {}";
        let buf = Vec::from(msg.as_bytes());
        let result = create_message_buffer(buf);
        assert!(matches!(result, Err(_)));
    }

    #[tokio::test]
    async fn test_finish_read_message_pass() {
        let first_msg = "{\"this\"";
        let remaining_msg = ": \"that\"";
        let expected_msg = format!("{}{}", first_msg, remaining_msg);
        let mut buffer: Vec<u8> = Vec::with_capacity(expected_msg.len());
        buffer.put(first_msg.as_bytes());
        let reader = tokio_test::io::Builder::default()
            .read(remaining_msg.as_bytes())
            .build();

        let mut codec = GnarloMsgCodec(reader);
        let result = codec.finish_read_message(buffer).await.unwrap();

        assert_eq!(result, expected_msg.as_bytes());
    }

    #[tokio::test]
    async fn test_finish_read_message_fail_eof() {
        let first_msg = "{\"this\"";
        let remaining_msg = ": \"that\"";
        let expected_msg = format!("{}{}", first_msg, remaining_msg);
        let mut buffer: Vec<u8> = Vec::with_capacity(expected_msg.len() + 1);
        buffer.put(first_msg.as_bytes());
        let reader = tokio_test::io::Builder::default()
            .read(remaining_msg.as_bytes())
            .build();

        let mut codec = GnarloMsgCodec(reader);
        let result = codec.finish_read_message(buffer).await;

        assert!(matches!(result, Err(_)));
    }
}
