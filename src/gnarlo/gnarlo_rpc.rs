use crate::api::GnarloApi;
use crate::gnarlo::gnarlo_codec::GnarloMsg;
use crate::gnarlo::gnarlo_messages as messages;
use crate::gnarlo::gnarlo_messages::RequestType;
use crate::rpc::{FrameMessage, Rpc};
use serde_json::Map;
use std::io;
use std::io::{Error, ErrorKind};
use std::net::SocketAddr;
use std::sync::atomic::{AtomicU32, Ordering};
use tokio::sync::mpsc::Receiver;
use tokio::sync::mpsc::Sender;

#[derive(Debug)]
pub struct GnarloRpc {
    api: GnarloApi,
    current_id: AtomicU32,
    egress_tx: Sender<FrameMessage<GnarloMsg>>,
}

impl GnarloRpc {
    pub fn new(egress_tx: Sender<FrameMessage<GnarloMsg>>, api: GnarloApi) -> Self {
        Self {
            egress_tx,
            api,
            current_id: AtomicU32::new(1),
        }
    }
    fn next_id(&self) -> u32 {
        return self.current_id.fetch_add(1, Ordering::SeqCst);
    }

    //A little haggard, I'd rather have this object handle the message framing
    //and invoke the API.  Then return the result back in *something like api.rs, but
    //different since this is specific to RPC, not the repository management.
    //perhaps this belongs in an "rpc", that then calls the api to store/retreive any data.
    fn on_status(
        &self,
        addr: SocketAddr,
        req: &messages::RequestType,
    ) -> io::Result<messages::ResponseType> {
        self.api.update_camera(addr, &req)?;
        return Ok(self.create_response(&req));
    }
    async fn on_registration(
        &mut self,
        addr: SocketAddr,
        req: &messages::RequestType,
    ) -> io::Result<messages::ResponseType> {
        self.api.update_camera(addr, &req)?;
        self.send_register_set(addr).await?;
        return Ok(self.create_response(&req));
    }
    fn on_unsupported(
        &self,
        addr: SocketAddr,
        msg: &messages::RequestType,
    ) -> io::Result<messages::ResponseType> {
        log::error!("Unsupported message type: {} from {}", msg.req_type, addr);
        return Ok(messages::ResponseType::new(self.next_id()));
    }
    fn on_error(
        &self,
        addr: SocketAddr,
        msg: &messages::RequestType,
        e: Error,
    ) -> messages::ResponseType {
        log::error!(
            "Encountered error message type: {} from {}: {}",
            msg.req_type,
            addr,
            e
        );
        return messages::ResponseType::new(self.next_id());
    }
    fn create_response(&self, req: &messages::RequestType) -> messages::ResponseType {
        return messages::ResponseType::new(req.id);
    }

    async fn send_request(&self, peer: SocketAddr, req: messages::RequestType) -> io::Result<()> {
        let json = match req.to_json() {
            Ok(s) => s,
            Err(e) => return Err(Error::new(ErrorKind::InvalidData, e)),
        };
        let msg = GnarloMsg::new(json);
        let frame = FrameMessage::new(msg, peer, true);
        match self.egress_tx.send(frame).await {
            Ok(()) => Ok(()),
            Err(e) => Err(Error::new(
                ErrorKind::ConnectionRefused,
                format!("Could not send to egress queue: {}", e),
            )),
        }
    }

    async fn send_register_set(&mut self, peer: SocketAddr) -> std::io::Result<()> {
        //{"Type":"registerSet","ID":26,"SetValues":
        let j = serde_json::json!({"VideoExposureCompensation":0,"VideoMirror":false,"VideoFlip":false,"VideoWindowStartX":0,"VideoWindowStartY":0,"VideoWindowEndX":1280,"VideoWindowEndY":720,"MaxMissedBeaconTime":10,"MaxStreamTimeLimit":1800,"VideoAntiFlickerRate":60
        ,"WifiCountryCode":"US","NightVisionMode":true,"HdrControl":"auto","MaxUserStreamTimeLimit":1800,"MaxMotionStreamTimeLimit":120,"VideoMode":"superWide","JPEGOutputResolution":"","ChargeNotificationLed":1,"AudioMicAGC":0,"VideoOutputResolution":"720p","VideoTargetBitrate":450,
        "Audio0EncodeFormat":0,"Audio1EncodeFormat":1,"ArloSmart":false,"AlertBackoffTime":0,"PIRTargetState":"Disarmed","AudioTargetState":"Disarmed","VideoMotionEstimationEnable":false,"DefaultMotionStreamTimeLimit":10
        });

        let mut map = Map::new();
        map.insert("SetValues".to_string(), j);
        let request =
            messages::RequestType::new("registerSet".to_string(), self.next_id(), Some(map));
        return self.send_request(peer, request).await;
    }
    async fn send_status_request(&mut self, peer: SocketAddr) -> std::io::Result<()> {
        log::debug!("Sending status request to {}", peer);
        let request = messages::RequestType::new("statusRequest".to_string(), self.next_id(), None);
        return self.send_request(peer, request).await;
    }

    pub async fn refresh_status(&mut self) -> std::io::Result<()> {
        log::debug!("Would have ran a heartbeat");
        for (serial, camera) in self.api.list_cameras()? {
            let ip = format!("{}:4000", camera.get_ip());
            log::debug!(
                "Preparing to send heartbeat to camera: {} at socket address: {}",
                serial.as_str(),
                ip
            );

            let peer: SocketAddr = match ip.parse() {
                Ok(p) => p,
                Err(e) => {
                    log::error!("Error parsing registered IP '{}': {}", ip, e);
                    continue;
                }
            };
            //send outbound status
            match self.send_status_request(peer).await {
                Ok(_) => {
                    log::debug!("Sent status request to camera: {}", ip);
                }
                Err(e) => {
                    log::error!("Error sending status request to {}: {}", ip, e);
                }
            };
        }
        Ok(())
    }

    fn decode_frame(&mut self, msg: GnarloMsg) -> io::Result<RequestType> {
        match RequestType::parse_json(msg.to_str()) {
            Ok(req) => Ok(req),
            Err(e) => Err(Error::new(
                ErrorKind::InvalidData,
                format!("Could not parse json: {}", e),
            )),
        }
    }
}

#[async_trait::async_trait]
impl Rpc<GnarloMsg> for GnarloRpc {
    fn create_egress_channel(
        queue_size: usize,
    ) -> (
        Sender<FrameMessage<GnarloMsg>>,
        Receiver<FrameMessage<GnarloMsg>>,
    ) {
        let channel = tokio::sync::mpsc::channel::<FrameMessage<GnarloMsg>>(queue_size);
        return channel;
    }

    async fn handle_request(
        &mut self,
        frame: FrameMessage<GnarloMsg>,
    ) -> io::Result<FrameMessage<GnarloMsg>> {
        let msg = frame.get_msg();
        let peer = frame.get_peer();

        let req = self.decode_frame(msg)?;
        //let mut future = None;

        let response = match req.req_type.as_str() {
            "registration" => self.on_registration(peer, &req).await,
            "status" => self.on_status(peer, &req),
            _ => self.on_unsupported(peer, &req),
        };
        let response = match response {
            Ok(response) => response,
            Err(e) => self.on_error(peer, &req, e),
        };

        let json = match response.to_json() {
            Ok(s) => s,
            Err(e) => return Err(Error::new(ErrorKind::InvalidData, e)),
        };
        let msg = GnarloMsg::new(json);
        Ok(FrameMessage::new(msg, peer, false))
    }

    async fn handle_egress_error(&mut self, e: Error, _: FrameMessage<GnarloMsg>) {
        log::error!("Egress error: {}", e);
    }

    async fn handle_response(&mut self, _: FrameMessage<GnarloMsg>, _: FrameMessage<GnarloMsg>) {}

    async fn handle_command(&mut self, command: String) {
        let result = match command.as_str() {
            "refresh_status" => self.refresh_status().await,
            _ => Err(Error::new(ErrorKind::Other, format!("Unknown command"))),
        };
        match result {
            Ok(_) => {
                log::info!("Command executed: {}", command);
            }
            Err(e) => {
                log::error!("handle_command, error executing: {};  {}", command, e);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::gnarlo::gnarlo_codec::GnarloMsg;
    use crate::repository::GnarloRepository;
    use tokio_test::assert_err;

    #[tokio::test]
    async fn test_next_id() {
        let repo = GnarloRepository::new();
        let api = GnarloApi::new(repo);
        let (tx, _) = GnarloRpc::create_egress_channel(1);
        let rpc = GnarloRpc::new(tx, api);

        assert_eq!(rpc.next_id(), 1);
        assert_eq!(rpc.next_id(), 2);
    }

    #[tokio::test]
    async fn test_handle_frame_passes_with_valid_type() {
        let json = String::from(
            "{\"ID\": 1, \"Type\": \"registration\", \"SystemSerialNumber\":\"XXXXXXXXXXXX\"}",
        );
        let msg = GnarloMsg::new(json);
        let repo = GnarloRepository::new();
        let api = GnarloApi::new(repo);

        let (tx, _) = GnarloRpc::create_egress_channel(1);
        let mut rpc = GnarloRpc::new(tx, api);

        let frame_msg = FrameMessage::new(msg, "0.0.0.0:0".parse().unwrap(), false);

        let result = match rpc.handle_request(frame_msg).await {
            Ok(s) => s,
            Err(e) => {
                panic!("There was an error: {}", e);
            }
        };
        let msg = result.get_msg();
        log::error!("result: {}", msg.to_str());
    }

    //these are wrong now because I still send a response.  I found it was worse to not send a
    //valid response type, due to the cameras recusing themselves from the network if they
    //do not like that response from the base station.

    #[tokio::test]
    async fn test_handle_frame_fails_with_invalid_type() {
        let repo = GnarloRepository::new();
        let api = GnarloApi::new(repo);

        let (tx, _) = GnarloRpc::create_egress_channel(1);
        let mut rpc = GnarloRpc::new(tx, api);

        let json = String::from("{\"ID\": \"id\", \"Type\": \"test\"}");
        let msg = GnarloMsg::new(json);
        let frame_msg = FrameMessage::new(msg, "0.0.0.0:0".parse().unwrap(), false);

        rpc.handle_request(frame_msg).await.unwrap();
    }

    #[tokio::test]
    async fn test_handle_frame_fails_with_invalid_json() {
        let repo = GnarloRepository::new();
        let api = GnarloApi::new(repo);

        let (tx, _) = GnarloRpc::create_egress_channel(1);
        let mut rpc = GnarloRpc::new(tx, api);

        let json = String::from("{\"ID\": \"id\", \"Type\": \"test\"");
        let msg = GnarloMsg::new(json);
        let frame_msg = FrameMessage::new(msg, "0.0.0.0:0".parse().unwrap(), false);
        let result = rpc.handle_request(frame_msg).await;
        assert_err!(result);
    }
}
