use std::net::SocketAddr;

use crate::api::GnarloApi;
use warp::Filter;

pub struct GnarloHttpService {
    api: GnarloApi,
}

pub async fn index_cameras(api: GnarloApi) -> Result<impl warp::Reply, warp::Rejection> {
    let cameras = match api.list_cameras() {
        Ok(c) => c,
        Err(_) => return Err(warp::reject()),
    };
    Ok(warp::reply::json(&cameras))
}

impl GnarloHttpService {
    pub fn new(api: GnarloApi) -> Self {
        return Self { api };
    }

    pub async fn run(&self, addr: SocketAddr) -> std::io::Result<()> {
        let api = self.api.clone();
        let state_filter = warp::any().map(move || api.clone());

        let list_cameras = warp::get()
            .and(warp::path("v1"))
            .and(warp::path("camera"))
            .and(warp::path::end())
            .and(state_filter.clone())
            .and_then(index_cameras);

        warp::serve(list_cameras).run(addr).await;
        return Ok(());
    }
}
