use crate::gnarlo::RequestType;
use crate::repository::{GnarloCamera, GnarloCameraMapType, GnarloRepository};
use std::io::Result;
use std::net::SocketAddr;

#[derive(Debug, Clone)]
pub struct GnarloApi {
    repo: GnarloRepository,
}

impl GnarloApi {
    pub(crate) fn list_cameras(&self) -> Result<GnarloCameraMapType> {
        return self.repo.list_cameras();
    }

    pub(crate) fn update_camera(&self, addr: SocketAddr, request: &RequestType) -> Result<()> {
        let camera = GnarloCamera::new(&addr, request)?;
        self.repo.update_camera(camera);
        Ok(())
    }
}

impl GnarloApi {
    pub fn new(repo: GnarloRepository) -> Self {
        return Self { repo };
    }
}
