use crate::FrameMessage;
use std::io::{Error, Result};
use tokio::sync::mpsc::Receiver;
use tokio::sync::mpsc::Sender;

#[async_trait::async_trait]
pub trait Rpc<M>
where
    M: Clone,
{
    fn create_egress_channel(
        queue_size: usize,
    ) -> (Sender<FrameMessage<M>>, Receiver<FrameMessage<M>>);
    async fn handle_request(&mut self, frame: FrameMessage<M>) -> Result<FrameMessage<M>>;
    async fn handle_egress_error(&mut self, e: Error, request: FrameMessage<M>);
    async fn handle_response(&mut self, request: FrameMessage<M>, response: FrameMessage<M>);
    async fn handle_command(&mut self, command: String);
}
