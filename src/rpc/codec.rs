use async_trait::async_trait;
use std::io::Result;
use std::net::SocketAddr;
use tokio::io::{AsyncRead, AsyncWrite};


#[derive(Clone, Debug)]
pub struct FrameMessage<M>
where M: Clone
{
    frame: M,
    peer: SocketAddr,
    expect_reply: bool
}

impl<M> FrameMessage<M>
where M: Clone
{

    pub fn get_msg(&self) -> M {
        return self.frame.clone();
    }
    pub fn get_peer(&self) -> SocketAddr {
        return self.peer.clone();
    }
    pub fn get_expect_reply(&self) -> bool {
        return self.expect_reply;
    }

    pub fn new(frame: M, peer: SocketAddr, expect_reply: bool) -> Self {
        Self {
            frame,
            peer,
            expect_reply
        }
    }
}


#[async_trait]
pub trait FrameCodec<M> : Sized
where M: Clone
{
    async fn shutdown(mut self) -> Result<()>;

    async fn read_frame(
        &mut self,
        peer: SocketAddr
    ) -> Result<FrameMessage<M>>;

    async fn write_frame(
        &mut self,
        frame: FrameMessage<M>,
    ) -> std::io::Result<()>;
}

pub trait FrameStream : AsyncWrite + AsyncRead + Unpin + Send {}

pub trait FrameCodecFactory<S, C, M>
    where
        M: Clone,
        S: FrameStream,
        C: FrameCodec<M> {

    fn create_codec(&self, stream: S) -> Result<C>;
}