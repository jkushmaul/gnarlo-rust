use crate::rpc::{FrameCodec, FrameCodecFactory, FrameStream, Rpc};
use crate::{FrameMessage, Server};
use std::io::{Error, ErrorKind, Result};
use std::marker::PhantomData;
use std::net::SocketAddr;
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::mpsc::Receiver;

impl FrameStream for TcpStream {}

pub struct TcpServer<C, M, F, R>
where
    M: Clone,
    C: FrameCodec<M>,
    F: FrameCodecFactory<TcpStream, C, M>,
    R: Rpc<M>,
{
    phantom_c: PhantomData<C>,
    phantom_m: PhantomData<M>,
    codec_factory: F,
    rpc: R,
    egress_rx: Receiver<FrameMessage<M>>,
    cron_rx: Receiver<String>,
}

impl<C, M, F, R> TcpServer<C, M, F, R>
where
    M: Clone,
    C: FrameCodec<M>,
    F: FrameCodecFactory<TcpStream, C, M>,
    R: Rpc<M>,
{
    pub fn new(
        codec_factory: F,
        rpc: R,
        egress_rx: Receiver<FrameMessage<M>>,
        cron_rx: Receiver<String>,
    ) -> Self {
        Self {
            phantom_c: PhantomData,
            phantom_m: PhantomData,
            codec_factory,
            rpc,
            egress_rx,
            cron_rx,
        }
    }

    async fn send_egress(&mut self, frame: FrameMessage<M>) -> Result<()> {
        let peer = frame.get_peer();
        let expect_reply = frame.get_expect_reply();
        let peer = SocketAddr::new(peer.ip(), 4000);
        log::info!("Connecting to {}", peer);
        let stream = TcpStream::connect(peer).await?;
        let mut codec = self.codec_factory.create_codec(stream)?;

        let frame2 = frame.clone();
        log::info!("Writing frame to {}", peer);
        codec.write_frame(frame).await?;

        if expect_reply {
            log::info!("reading frame from {}", peer);
            let response = codec.read_frame(peer).await?;
            log::info!("handling response from {}", peer);
            self.rpc.handle_response(frame2, response).await;
        }
        Ok(())
    }

    async fn handle_command(&mut self, command: Option<String>) -> Result<()> {
        log::info!("Handling command");
        if command.is_none() {
            return Err(Error::new(ErrorKind::Other, "Received none from cron"));
        }
        self.rpc.handle_command(command.unwrap()).await;
        Ok(())
    }

    async fn handle_egress(&mut self, frame: Option<FrameMessage<M>>) -> Result<()> {
        log::info!("Handling egress");
        if frame.is_none() {
            return Err(Error::new(ErrorKind::Other, "Received none from egress"));
        }
        let frame = frame.unwrap();
        let peer = frame.get_peer();
        let frame2 = frame.clone();
        match self.send_egress(frame).await {
            Ok(_) => {}
            Err(e) => {
                log::error!("Error sending egress to {}: {}", peer, e);
                self.rpc.handle_egress_error(e, frame2).await;
            }
        };
        Ok(())
    }

    async fn handle_peer(&mut self, peer: SocketAddr, stream: TcpStream) -> Result<()> {
        let mut codec: C = self.codec_factory.create_codec(stream)?;
        let request = codec.read_frame(peer).await?;
        let response = self.rpc.handle_request(request).await?;
        codec.write_frame(response).await?;
        codec.shutdown().await?;
        Ok(())
    }

    async fn handle_tcp(&mut self, result: Result<(TcpStream, SocketAddr)>) -> Result<()> {
        let (stream, peer) = result?;
        log::info!("Accepted connection from {}", peer);
        match self.handle_peer(peer, stream).await {
            Ok(_) => {}
            Err(e) => {
                log::error!("Error handling peer '{}': {}", peer, e);
            }
        };
        Ok(())
    }
}

#[async_trait::async_trait]
impl<C, M, F, R> Server<C, M, TcpStream> for TcpServer<C, M, F, R>
where
    M: Clone + Send,
    C: FrameCodec<M> + Send,
    F: FrameCodecFactory<TcpStream, C, M> + Send,
    R: Rpc<M> + Send,
{
    async fn run(mut self, addr: SocketAddr) -> Result<()> {
        log::info!("listening on tcp://{}", addr);
        let listener = TcpListener::bind(addr).await?;

        loop {
            let result = tokio::select! {
                result  = self.cron_rx.recv() => {
                    self.handle_command(result).await
                },
                result = self.egress_rx.recv() => {
                    self.handle_egress(result).await
                },
                result = listener.accept() => {
                    self.handle_tcp(result).await
                }, //
            };
            if result.is_err() {
                break;
            }
        }
        Ok(())
    }
}
