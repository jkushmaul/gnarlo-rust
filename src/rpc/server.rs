use crate::rpc::{FrameCodec, FrameStream};
use std::io::Result;
use std::net::SocketAddr;

#[async_trait::async_trait]
pub trait Server<C, M, S>
where
    M: Clone,
    C: FrameCodec<M>,
    S: FrameStream,
{
    async fn run(mut self, addr: SocketAddr) -> Result<()>;
}
