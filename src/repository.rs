use crate::gnarlo::RequestType;
use lru::LruCache;
use serde::Serialize;
use std::collections::HashMap;
use std::io::{Error, ErrorKind, Result};
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};

const MAX_CAMS: usize = 255;

#[derive(Debug, Clone, Serialize, Eq, Hash, PartialEq)]
pub struct GnarloCameraSerial {
    serial: String,
}
impl GnarloCameraSerial {
    pub fn as_str(&self) -> &str {
        return self.serial.as_str();
    }
    pub fn from(req: &RequestType) -> Result<Self> {
        let serial = match req.get_serial() {
            None => {
                return Err(Error::new(
                    ErrorKind::InvalidData,
                    "request did not contain serial",
                ))
            }
            Some(s) => s.to_string(),
        };
        Ok(Self { serial })
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct GnarloCamera {
    serial: GnarloCameraSerial,
    ip: String,
    last_request: RequestType,
}
impl GnarloCamera {
    pub fn get_ip(&self) -> String {
        return self.ip.clone();
    }
}

type GnarloCameraCache = LruCache<GnarloCameraSerial, GnarloCamera>;
pub(crate) type GnarloCameraMapType = HashMap<GnarloCameraSerial, GnarloCamera>;

#[derive(Debug, Clone)]
pub struct GnarloRepository {
    cameras: Arc<Mutex<GnarloCameraCache>>,
}

impl GnarloCamera {
    pub fn new(addr: &SocketAddr, request_type: &RequestType) -> Result<Self> {
        let ip: String = addr.ip().to_string();
        let serial = GnarloCameraSerial::from(&request_type)?;
        let last_request: RequestType = request_type.clone();
        Ok(Self {
            serial,
            ip,
            last_request,
        })
    }
}

// I just want to apologize to the greater community, right now.
// In the future I'd do something like diesel, but I needed prototype

impl GnarloRepository {
    pub(crate) fn list_cameras(&self) -> Result<GnarloCameraMapType> {
        let results = self
            .cameras
            .lock()
            .unwrap()
            .iter()
            .map(|a| (a.0.clone(), a.1.clone()))
            .collect();
        return Ok(results);
    }
    pub(crate) fn update_camera(&self, camera: GnarloCamera) {
        let key = camera.serial.clone();
        self.cameras.lock().unwrap().put(key, camera);
    }
}

impl GnarloRepository {
    pub fn new() -> Self {
        let cameras: GnarloCameraCache = LruCache::new(MAX_CAMS);
        let cameras = Arc::new(Mutex::new(cameras));
        return Self { cameras };
    }
}

#[cfg(test)]
mod tests {
    use crate::gnarlo::RequestType;
    use crate::repository::GnarloCamera;
    use serde_json::{Map, Value};
    use std::net::SocketAddr;

    #[tokio::test]
    async fn test_get_ip() {
        let mut metadata = Map::new();
        metadata.insert(
            "SystemSerialNumber".to_string(),
            Value::String("serial".to_string()),
        );
        let req = RequestType::new("test".to_string(), 1, Some(metadata));
        let peer: SocketAddr = "0.0.0.0:1234".parse().unwrap();
        let cam = GnarloCamera::new(&peer, &req).unwrap();
        let ip = cam.get_ip();
        assert_eq!("0.0.0.0", ip);
    }
}
